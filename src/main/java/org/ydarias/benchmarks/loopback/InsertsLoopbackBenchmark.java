package org.ydarias.benchmarks.loopback;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.gson.Gson;
import okhttp3.*;
import org.ydarias.benchmarks.loopback.model.CoffeeShop;
import org.ydarias.benchmarks.loopback.model.CoffeeShopProductPair;
import org.ydarias.benchmarks.loopback.model.Comment;
import org.ydarias.benchmarks.loopback.model.Product;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class InsertsLoopbackBenchmark {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private final OkHttpClient client = new OkHttpClient();

    private final Gson gson = new Gson();

    private final Timer coffeeShopInserts;

    private final Timer productInserts;

    private final Timer commentInserts;

    public InsertsLoopbackBenchmark(MetricRegistry metrics) {
        coffeeShopInserts = metrics.timer("CoffeeShop inserts");
        productInserts = metrics.timer("Product inserts");
        commentInserts = metrics.timer("Comment inserts");
    }

    public Set<CoffeeShopProductPair> execute(int times) {
        return IntStream.range(0, times).boxed()
                .map(iteration -> test())
                .collect(Collectors.toSet());
    }

    private CoffeeShopProductPair test() {
        try {
            CoffeeShop coffeeShop = postCoffeeShop();

            Product product = postProduct(coffeeShop);
            postProduct(coffeeShop);

            postComment(coffeeShop);
            postComment(coffeeShop);
            postComment(coffeeShop);

            return CoffeeShopProductPair.Builder.aCoffeeShopProductPair()
                    .withCoffeeShopId(coffeeShop.getId())
                    .withProductId(product.getId())
                    .build();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private CoffeeShop postCoffeeShop() throws IOException {
        final Timer.Context coffeeShopContext = coffeeShopInserts.time();
        Response currentResponse = null;
        CoffeeShop coffeeShopParsedResponse;

        try {
            CoffeeShop coffeeShop = CoffeeShop.Builder.aCoffeeShop()
                    .withName("My test coffee shop")
                    .withCity("Madrid")
                    .build();

            RequestBody body = RequestBody.create(JSON, gson.toJson(coffeeShop));

            Request postCoffeeShop = new Request.Builder()
                    .url("http://localhost:3000/api/CoffeeShops")
                    .post(body)
                    .build();

            currentResponse = client.newCall(postCoffeeShop).execute();
            coffeeShopParsedResponse = gson.fromJson(currentResponse.body().string(), CoffeeShop.class);
        } finally {
            coffeeShopContext.stop();
            if (currentResponse != null)
                currentResponse.close();
        }

        return coffeeShopParsedResponse;
    }

    private Product postProduct(CoffeeShop coffeeShop) throws IOException {
        final Timer.Context productContext = productInserts.time();
        Response currentResponse = null;
        Product productParsedResponse;

        try {
            String url = String.format("http://localhost:3000/api/CoffeeShops/%s/products", coffeeShop.getId());

            Product product = Product.Builder.aProduct()
                    .withName("My product")
                    .withType("A type")
                    .build();

            RequestBody body = RequestBody.create(JSON, gson.toJson(product));

            Request postComment = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            currentResponse = client.newCall(postComment).execute();
            productParsedResponse = gson.fromJson(currentResponse.body().string(), Product.class);
        } finally {
            productContext.stop();
            if (currentResponse != null)
                currentResponse.close();
        }

        return productParsedResponse;
    }

    private void postComment(CoffeeShop coffeeShop) throws IOException {
        final Timer.Context commentContenxt = commentInserts.time();
        Response currentResponse = null;

        try {
            String url = String.format("http://localhost:3000/api/CoffeeShops/%s/comments", coffeeShop.getId());

            Comment comment = Comment.Builder.aComment()
                    .withPayload("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ut")
                    .withAuthor("The author")
                    .build();

            RequestBody body = RequestBody.create(JSON, gson.toJson(comment));

            Request postComment = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();

            currentResponse = client.newCall(postComment).execute();
        } finally {
            commentContenxt.stop();
            if (currentResponse != null)
                currentResponse.close();
        }
    }

}
