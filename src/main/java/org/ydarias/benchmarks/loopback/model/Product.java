package org.ydarias.benchmarks.loopback.model;

public class Product {

    private String id;

    private String name;

    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static final class Builder {
        private String name;
        private String type;

        private Builder() {
        }

        public static Builder aProduct() {
            return new Builder();
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Product build() {
            Product product = new Product();
            product.type = this.type;
            product.name = this.name;
            return product;
        }
    }
}
