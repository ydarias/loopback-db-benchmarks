package org.ydarias.benchmarks.loopback.model;

public class Comment {

    private String payload;

    private String author;

    public static final class Builder {
        private String payload;
        private String author;

        private Builder() {
        }

        public static Builder aComment() {
            return new Builder();
        }

        public Builder withPayload(String payload) {
            this.payload = payload;
            return this;
        }

        public Builder withAuthor(String author) {
            this.author = author;
            return this;
        }

        public Comment build() {
            Comment comment = new Comment();
            comment.author = this.author;
            comment.payload = this.payload;
            return comment;
        }
    }
}
