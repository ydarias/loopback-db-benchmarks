package org.ydarias.benchmarks.loopback.model;

public class CoffeeShopProductPair {

    private String coffeeShopId;

    private String productId;

    public String getCoffeeShopId() {
        return coffeeShopId;
    }

    public void setCoffeeShopId(String coffeeShopId) {
        this.coffeeShopId = coffeeShopId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public static final class Builder {
        private String coffeeShopId;
        private String productId;

        private Builder() {
        }

        public static Builder aCoffeeShopProductPair() {
            return new Builder();
        }

        public Builder withCoffeeShopId(String coffeeShopId) {
            this.coffeeShopId = coffeeShopId;
            return this;
        }

        public Builder withProductId(String productId) {
            this.productId = productId;
            return this;
        }

        public CoffeeShopProductPair build() {
            CoffeeShopProductPair coffeeShopProductPair = new CoffeeShopProductPair();
            coffeeShopProductPair.setCoffeeShopId(coffeeShopId);
            coffeeShopProductPair.setProductId(productId);
            return coffeeShopProductPair;
        }
    }
}
