package org.ydarias.benchmarks.loopback.model;

public class CoffeeShop {

    private String id;

    private String name;

    private String city;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public static final class Builder {
        private String name;
        private String city;

        private Builder() {
        }

        public static Builder aCoffeeShop() {
            return new Builder();
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withCity(String city) {
            this.city = city;
            return this;
        }

        public CoffeeShop build() {
            CoffeeShop coffeeShop = new CoffeeShop();
            coffeeShop.city = this.city;
            coffeeShop.name = this.name;
            return coffeeShop;
        }
    }

}
