package org.ydarias.benchmarks.loopback;

import com.codahale.metrics.ConsoleReporter;
import com.codahale.metrics.MetricRegistry;
import org.ydarias.benchmarks.loopback.model.CoffeeShopProductPair;

import java.util.Set;
import java.util.concurrent.TimeUnit;

public class BenchmarkLauncher {

    private static final MetricRegistry metrics = new MetricRegistry();

    public static void main(String ... args) {
        ConsoleReporter reporter = createReporter();

        InsertsLoopbackBenchmark insertsBenchmark = new InsertsLoopbackBenchmark(metrics);
        Set<CoffeeShopProductPair> insertedItems = insertsBenchmark.execute(1_000);

        UpdatesLoopbackBenchmark updatesLoopbackBenchmark = new UpdatesLoopbackBenchmark(metrics);
        updatesLoopbackBenchmark.execute(100, insertedItems);

        reporter.report();
        reporter.stop();
    }

    private static ConsoleReporter createReporter() {
        ConsoleReporter reporter = ConsoleReporter.forRegistry(metrics)
                .convertRatesTo(TimeUnit.SECONDS)
                .convertDurationsTo(TimeUnit.MILLISECONDS)
                .build();
        reporter.start(30, TimeUnit.SECONDS);

        return reporter;
    }

}
