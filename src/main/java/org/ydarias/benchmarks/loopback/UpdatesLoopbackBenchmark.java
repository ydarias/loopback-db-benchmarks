package org.ydarias.benchmarks.loopback;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.google.gson.Gson;
import okhttp3.*;
import org.ydarias.benchmarks.loopback.model.CoffeeShop;
import org.ydarias.benchmarks.loopback.model.CoffeeShopProductPair;
import org.ydarias.benchmarks.loopback.model.Product;

import java.io.IOException;
import java.util.Set;

public class UpdatesLoopbackBenchmark {

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    private final OkHttpClient client = new OkHttpClient();

    private final Gson gson = new Gson();

    private final Timer productUpdates;

    public UpdatesLoopbackBenchmark(MetricRegistry metrics) {
        productUpdates = metrics.timer("Product updates");
    }

    public void execute(int times, Set<CoffeeShopProductPair> insertedItems) {
        insertedItems.stream()
                .limit(times)
                .forEach(item -> putProduct(item));
    }

    private void putProduct(CoffeeShopProductPair item) {
        final Timer.Context productContext = productUpdates.time();
        Response currentResponse = null;

        try {
            String url = String.format("http://localhost:3000/api/CoffeeShops/%s/products/%s",
                    item.getCoffeeShopId(), item.getProductId());

            Product product = Product.Builder.aProduct()
                    .withName("My UPDATED product")
                    .withType("An updated type")
                    .build();

            RequestBody body = RequestBody.create(JSON, gson.toJson(product));

            Request postComment = new Request.Builder()
                    .url(url)
                    .put(body)
                    .build();

            currentResponse = client.newCall(postComment).execute();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            productContext.stop();
            if (currentResponse != null)
                currentResponse.close();
        }
    }

}
